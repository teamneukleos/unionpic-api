<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
    protected $fillable = ['image_id', 'tag'];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }
}