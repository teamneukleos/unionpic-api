<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    protected $fillable = [
        'name', 'image_link', 'copyright', 'tags', 'thumblink'
    ];

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }

}


