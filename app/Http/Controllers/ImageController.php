<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Helpers\Upload;
use App\Tag;

use Validator, Redirect, Response, File;


class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->file = new Upload;
        $this->image = new Image;
    }

    //

    public function getImages()
    {
        return response()->json([
            "Message" => "Images",
            "data" => Image::all()
        ], 200);
    }

    public function getOneImage($id)
    {
        return response()->json([
            "Message" => "Image",
            "data" => Image::find($id)
        ], 200);
    }

    public function postImage(Request $request)
    {
        $data = $request->only(['image', 'name', 'copyright', 'tags',]);
        $this->image->image_link = $this->file->upload($data["image"]);
        $this->image->thumblink = $this->file->createThumbnail($data['image'], "thumbnails/{$data['name']}", 100, 100);
        $this->image->name = $data['name'];
        $this->image->copyright = $data['copyright'];
        $this->image->tags = $data['tags'];
        $this->image->save();
        return response()->json([
            "Message" => 'Image Uploaded',
            'data' => $this->image
        ], 200);
    }

    // public function updateImage(Request $request, $id){
    //     $data = $request->only(['image', 'name', 'copyright', 'tags',]);
    //     $image = $this->image->find($id);
    //     $image->copyright = $data['copyright'];
    //     $image->name = $data['name'];
    //     $image->tags = $data['tags'];
    //     if($data["image"] != "..."){
    //     $image->image_link = $this->file->update($data["image"], $image->image_link);
    //     $image->thumblink = $this->file->createThumbnail($data['image'], "thumbnails/{$data['name']}",100,100);
    //     }
    //     $image->save();
    //     return response()->json([
    //         "Message" => 'Image Updated',
    //         'data' => $image
    //     ],200);
    // }

    public function deleteImage($id)
    {
        Tag::where('image_id', '=', $id)->delete();
        $image = $this->image->find($id);
        $image->delete();
    }



    public function imageUpload(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
            'copyright' => 'required',
            'tags' => 'required',
        ]);

        $data = $request->only(['image', 'name', 'copyright', 'tags']);
        $this->image->image_link = $this->file->upload($data["image"]);
        $this->image->thumblink = $this->file->createThumbnail($data['image'], 'thumbnails/' . uniqid() . 'thumbnail', 100, 100);
        $this->image->name = $data['name'];
        $this->image->copyright = $data['copyright'];
        $this->image->tags = $data['tags'];
        $this->image->save();


        // CREATE MULTIPLE TAGS, IN TAG TABLE
        $str_arr = explode(",", $data['tags']);
        $file_count = count($str_arr);

        for ($i = 0; $i < $file_count; $i++) {
            $tagname = trim($str_arr[$i]);

            Tag::create([
                'image_id' => $this->image->id,
                'tag' => $tagname
            ]);
        }

        return response()->json([
            "Message" => 'Image Uploaded',
            'data' => $this->image
        ], 200);
    }


    public function imageDelete($id)
    {
        Tag::where('image_id', '=', $id)->delete();
        Image::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }


    public function updateImage(Request $request, $id)
    {
        $data = $request->only(['image', 'copyright', 'tags',]);
        $image = $this->image->find($id);
        $image->copyright = $data['copyright'];
        // $image->name = $data['name'];
        $image->tags = $data['tags'];
        if ($data["image"] != "...") {
            $image->image_link = $this->file->update($data["image"], $image->image_link);
            $this->image->thumblink = $this->file->createThumbnail($data['image'], 'thumbnails/' . uniqid() . 'thumbnail', 100, 100);
        }
        $image->save();

        if ($data["tags"] != "...") {
            Tag::where('image_id', '=', $id)->delete();
        }

        // CREATE MULTIPLE TAGS, IN TAG TABLE
        $str_arr = explode(",", $data['tags']);
        $file_count = count($str_arr);

        for ($i = 0; $i < $file_count; $i++) {
            $tagname = trim($str_arr[$i]);

            Tag::create([
                'image_id' => $id,
                'tag' => $tagname
            ]);
        }

        return response()->json([
            "Message" => 'Image Updated',
            'data' => $image
        ], 200);
    }


    public function search(Request $request, $keyword)
    {

        if ($keyword != '') {
            $search_tags = Tag::has('image')->where('tag', '=', $keyword)
                ->pluck('image_id');

            $search_results = Image::whereIn('id', $search_tags)
                ->get();

            return response()->json($search_results, 200);
        }
    }

    public function downloadImage($id)
    {
        $image = $this->image->find($id);
        $pathToFile = base_path('public') . $image->image_link;
        return response()->download($pathToFile, $image->name);
    }
}
