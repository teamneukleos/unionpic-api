<?php


namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    //

    public function create(Request $request)
    {
        $data = $request->only(['name', 'email', 'password']);
        $this->user->name = $data['name'];
        $this->user->email = $data['email'];
        $this->user->password = Hash::make($data['password']);
        $this->user->save();
        return response()->json([
            "Message" => "User Created",
            "data" => $this->user
        ], 200);
    }

    public function login(Request $request)
    {
        $data = $request->only(['email', 'password']);
        $user = $this->user->where('email', $data['email'])->first();

        if (!$user || !Hash::check($data['password'], $user->password)) {
            return null;
        }

        $token = base64_encode(Str::random(40));
        $user->api_token = $token;
        $user->save();
        return  response()->json([
            "Message" => "Token",
            "data" => [
                "token" => $user->api_token,
                "role" => $user->role,
            ],
        ], 200);
    }

    public function fetch()
    {
        $id = Auth::id();
        $user = $this->user->find($id);
        return  response()->json([
            "Message" => "user",
            "data" => $user
        ], 200);
    }

    public function update(Request $request)
    {
        $id = Auth::id();
        $data = $request->only(['name', 'email', 'password']);
        $user = $this->user->find($id);
        $existingUser = $this->user->where('email', $data['email'])->first();
        if ($existingUser && $existingUser->id != $id) {
            return null;
        }
        if (!$user) {
            return null;
        }
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->save();
        return  response()->json([
            "Message" => "Token",
            "data" => $user
        ], 200);
    }

    public function updatePassword(Request $request)
    {
        $id = Auth::id();
        $data = $request->only(['current_password', 'password', 'password_confirmation']);
        $user = $this->user->where('id', $id)->first();

        if (!$user || !Hash::check($data['current_password'], $user->password)) {
            return false;
        }

        // $exist_password = Auth::User()->password;
        // if(!Hash::check($data['current_password'], $exist_password)){ return false; }
        // $user_id = Auth::User()->id;
        // $user = User::find($user_id);

        $user->password = Hash::make($data['password']);
        $user->save();
        return  response()->json([
            "Message" => "Updated Password",
            "data" => $user
        ], 200);
    }

    public function sendPasswordReset(Request $request)
    {

        try {
            $data = $request->only(['email']);
            $user = $this->user->where('email', $data['email'])->first();
            if (!$user) {
                return response()->json([
                    "message" => "Password reset sent",
                    "data" => null
                ], 200);
            }

            $user->reset_token = Str::random(40);
            $user->save();

            $email = Auth::email();
            $url = env("CLIENT_URL") . "password/" . $user->reset_token;

            // send email
            $txt = 'Hi there,You have requested a password reset. Please follow the link below to reset your password.'
                . $url;
            $headers = "From: donotreply@unionpic.com";
            Mail::raw($txt, function ($message) use ($email) {
                $message->to($email, 'UnionPic')->subject('Reset your Password');
            });
            return response()->json([
                "message" => 'Password reset sent',
                'data' => null
            ], 200);
        } catch (\Throwable $th) {
            abort(400, "Password could not be reset:" . $th->getMessage());
        }
    }

    public function resetPassword(Request $request, $token)
    {
        try {
            $data = $request->only(['password', 'password_confirmation']);

            $this->validate($request, [
                'password' => 'bail|required|confirmed',
            ], [
                'password.required' => 'The user\'s password is required',
                'password.confirmed' => 'The user\'s password confirmation is incorrect',
            ]);

            $user = $this->user->where('reset_token', $token)->first();
            $user->password = Hash::make($data['password']);
            $user->reset_token = null;
            $user->save();

            return response()->json([
                "message" => "Password changes",
                "data" => $user
            ], 200);
        } catch (\Throwable $th) {
            abort(400, "Password could not be reset" . $th->getMessage());
        }
    }

    public function delete()
    {
        $id = Auth::id();
        $this->user->destroy($id);
        return response()->json([
            "Message" => "User Deleted"
        ], 200);
    }
}
