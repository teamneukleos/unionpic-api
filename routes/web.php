<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function ($router) {

    $router->group(['prefix' => ''], function () use ($router) {
        $router->get('image/', [
            'uses' => 'ImageController@getImages',
        ]);
        $router->get('image/{id}', [
            'uses' => 'ImageController@getOneImage',
        ]);
        $router->post('image/create', [
            'uses' => 'ImageController@postImage',
            'middleware' => 'auth'
        ]);
        $router->post('image/{id}', [
            'uses' => 'ImageController@updateImage',
            'middleware' => 'auth'
        ]);
        $router->delete('image/{id}', [
            'uses' => 'ImageController@deleteImage',
            'middleware' => 'auth'
        ]);

        $router->get('image/download/{id}', [
            'uses' => 'ImageController@downloadImage',
            'middleware' => 'auth'
        ]);
        $router->post('api/imageUpload',  [
            'uses' => 'ImageController@imageUpload'
        ]);
        $router->post('api/imageUpdate/{id}',  [
            'uses' => 'ImageController@updateImage'
        ]);
        $router->delete('api/image/{id}', [
            'uses' => 'ImageController@imageDelete'
        ]);
        $router->post('api/imageSearch/{keyword}', [
            'uses' => 'ImageController@search'
        ]);
    });

    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('/create', [
            'uses' => 'UserController@create',
        ]);
        $router->post('/login', [
            'uses' => 'UserController@login',
        ]);
        $router->get('/', [
            'uses' => 'UserController@fetch',
            'middleware' => 'auth'
        ]);
        $router->put('/update', [
            'uses' => 'UserController@update',
            'middleware' => 'auth'
        ]);
        $router->put('/password/update', [
            'uses' => 'UserController@updatePassword',
            'middleware' => 'auth'
        ]);
        $router->delete('/delete', [
            'uses' => 'UserController@delete',
            'middleware' => 'auth'
        ]);
        $router->post('/password/forgot', [
            'uses' => 'UserController@sendPasswordReset',
        ]);
        $router->post('password/reset/{token}', [
            'uses' => 'UserController@resetPassword',
        ]);
    });
});
